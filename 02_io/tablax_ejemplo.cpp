#include <stdio.h>
#include <stdlib.h>

int main(){

	int op1=1, tabla;


	printf("¿Qué tabla quieres?\n");
	printf("Tabla: ");
	scanf(" %i", &tabla);

	printf("%ix1=%i\n", tabla, 1 * tabla);
	op1++;
	printf("%ix2=%i\n", tabla, 2 * tabla);
	op1++;
	printf("%ix3=%i\n", tabla, 3 * tabla);

	return EXIT_SUCCESS;
} 
