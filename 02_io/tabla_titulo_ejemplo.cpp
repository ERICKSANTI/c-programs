#include <stdio.h>
#include <stdlib.h>

#define MAX 0X100

void pon_titulo(int tabla){/*parametro formal*/ /*(esta parte esta la declaración de la funcioń)*/
	sprintf(titulo, "toilet -fpagga --metal Tabla del %i", tabla);  /* esta es la parte de la definición de la función*/
	system(titulo); /*esta es la llamada de la función*/
}

/* Función punto de entrada*/
int main(){

	/*declaración de variables */
	int op1=1, tabla;
	char titulo[MAX];

	/*MENU */
	printf("¿Qué tabla quieres?\n");
	printf("Tabla: ");
	scanf(" %i", &tabla);
	
	pon_titulo(tabla); /*llamada con parámetro actual 5 */
	
	/*Resultados*/
	printf("%ix1=%i\n", tabla, 1 * tabla);
	op1++;
	printf("%ix2=%i\n", tabla, 2 * tabla);
	op1++;
	printf("%ix3=%i\n", tabla, 3 * tabla);

	return EXIT_SUCCESS;
} 
